# The Global Tsunami Hazard Map

**SC leader:** <img src="https://gitlab.com/cheese5311126/CHEESE/-/raw/main/Logos/INGV.png?inline=false" width="40">, <img src="https://gitlab.com/cheese5311126/CHEESE/-/raw/main/Logos/UMA.png?inline=false" width="100">

**Partners:** <img src="https://gitlab.com/cheese5311126/CHEESE/-/raw/main/Logos/NGI.png?inline=false" width="60">, <img src="https://gitlab.com/cheese5311126/CHEESE/-/raw/main/Logos/TUM.png?inline=false" width="100">

**Codes:** [Tsunami-HySEA](https://gitlab.com/cheese5311126/codes/hysea/tsunami-hysea)

**Target EuroHPC Architectures:** Leonardo, MN5

**Type:** Hazard assessment

## Description

This SC builds on the functionality developed in PD3. Its objective is to provide a
global seismic tsunami hazard map at unprecedented resolution, in terms of both
overall source variability exploration and spatial output for specific sites. The
model aims at a full coverage of both global tsunamigenic earthquake zones and
global coastlines. It will exploit previously developed global tsunami hazard
analysis and will substantially extend the resolution and uncertainty treatment to
provide a new global benchmark. High resolution tsunami modelling to produce
probabilistic inundation maps is possible after selection of sources that are
significant for a specific site. The modelling tools will be reusable with different
inputs, and will be made interoperable with standard source and hazard
descriptions, also towards multi-hazard risk assessment.

<img src="SC3.1.png">

**Figure 3.5.1.** Graphical abstract of SC3.1.

## Expected results

Capacity simulations will be carried out for about 100,000 unit sources carried out on regional to global
grids with resolutions of at least 30 arcseconds to simulate about 40 h of tsunami propagation. Complementary scenarios
(combining many unit sources) will be carried out as input to the local-scale inundation simulations. About 0.5M of GPU
hours for the offshore tsunami simulations and up to 5M of GPU hours for the inundation simulations are expected to
encompass several tens of millions of tsunami scenarios covering the entire globe and high resolution inundation maps for at
least 10-20 hotspot locations. This global benchmark, which will be distributed through the TCS Tsunami in EPOS, will
prototype possibilities for increased utilization of EuroHPC resources for local hazard and risk analysis, and form the
background for a unique service, to be added to those distributed by Geo-INQUIRE.